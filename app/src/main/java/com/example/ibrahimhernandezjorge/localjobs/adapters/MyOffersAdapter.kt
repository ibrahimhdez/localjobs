package com.example.ibrahimhernandezjorge.localjobs.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.utils.StateOffers

class MyOffersAdapter(offers: ArrayList<StateOffers>): RecyclerView.Adapter<MyOffersAdapter.ViewHolderOffer>() {
    private var myOffers = offers
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOffer {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_offer, parent, false)

        context = view.context

        return ViewHolderOffer(view)
    }

    override fun onBindViewHolder(holder: ViewHolderOffer, position: Int) {
        holder.offerType.text = myOffers[position].type
        holder.offerCompany.text = myOffers[position].company

        setOfferIcon(holder.stateIcon, myOffers[position].state)
    }

    override fun getItemCount(): Int {
        return myOffers.size
    }

    private fun setOfferIcon(icon: ImageView, state: String) {
        when(state) {
            Utils.INSCRIT -> {
                val imageDrawable = context!!.resources.getDrawable(R.drawable.ic_inscrit)

                icon.setImageDrawable(imageDrawable)
            }

            Utils.SELECTED -> {
                val imageDrawable = context!!.resources.getDrawable(R.drawable.ic_ok)

                icon.setImageDrawable(imageDrawable)
            }

            Utils.DISCARD -> {
                val imageDrawable = context!!.resources.getDrawable(R.drawable.ic_discard)

                icon.setImageDrawable(imageDrawable)
            }

        }
    }

    inner class ViewHolderOffer(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var stateIcon: ImageView = itemView.findViewById(R.id.state_icon_userOfOffers)
        var offerType: TextView = itemView.findViewById(R.id.name_userOfOffer)
        var offerCompany: TextView = itemView.findViewById(R.id.offer_company)
    }
}
