package com.example.ibrahimhernandezjorge.localjobs.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.example.ibrahimhernandezjorge.localjobs.controllers.CloudController
import com.example.ibrahimhernandezjorge.localjobs.controllers.LocationController
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.asyncTasks.AsyncResponse
import com.example.ibrahimhernandezjorge.localjobs.utils.Company
import com.example.ibrahimhernandezjorge.localjobs.utils.Offer
import com.example.ibrahimhernandezjorge.localjobs.controllers.OffersController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import java.io.IOException

class MapsActivity : AppCompatActivity(),
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener, OnMapReadyCallback,
        View.OnKeyListener,
        View.OnCreateContextMenuListener,
        OnSuccessListener<Location>,
        AsyncResponse,
        GoogleMap.OnMarkerClickListener{

    private var cloud = CloudController
    private var locationController: LocationController? = null
    private var offersController = OffersController()
    private var map: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    private var geocoder: Geocoder? = null
    private var wantedAddress: LatLng? = null
    private var editTextAddress: EditText? = null
    private var locationProvider: FusedLocationProviderClient? = null
    private var currentLocation: Location? = null
    private var filterFab: FloatingActionButton? = null
    private var markers = ArrayList<Marker>()
    private var menu: Menu? = null
    private var currentUserId = -1

    companion object {
        private const val REQUEST_LOCATION_PERMISSIONS_CODE = 111
        private const val EMPTY = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        initComponentes()
    }

    private fun initComponentes() {
        currentUserId = intent.extras!!.getInt(Utils.ID)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        locationController = LocationController(this, ::checkCompanyProximity)
        locationProvider = LocationServices.getFusedLocationProviderClient(this)
        geocoder = Geocoder(applicationContext)
        filterFab = findViewById(R.id.filter_fab)
        filterFab!!.setOnClickListener {
            showOfferFilterDialog()
        }

        requestLocationPermissions()
    }

    @SuppressLint("MissingPermission")
    private fun startApplication() {
        mapFragment!!.getMapAsync(this)
        locationProvider!!.lastLocation.addOnSuccessListener(this)
        locationController!!.createLocationRequest()
        locationController!!.initLocationCallback()
        locationController!!.initLocationReception(applicationContext)
        initCloudDownloads()
    }

    private fun initCloudDownloads() {
        cloud.delegate = this
        cloud.startApplication(currentUserId)
    }

    /**
     * Método onRequestPermissionResult para obtener la respuesta del usuario a la solicitud de los permisos.
     * Si el usuario ha aceptado los permisos, llamamos al método solicitarPermisosGeolocalización, ya con
     * los permisos concedidos y ejecutándose getMapa().setMyLocationEnabled(true). Si no los acepta, mostramos
     * un dialog fragment para notificar la necesidad de tener los permisos de localización.
     */
    override fun onRequestPermissionsResult(respuestaSolicitud: Int, permisos: Array<String>, resultados: IntArray) {
        if (respuestaSolicitud == REQUEST_LOCATION_PERMISSIONS_CODE) {
            if (resultados.isNotEmpty() && resultados[0] == PackageManager.PERMISSION_GRANTED) {
                startApplication()
            }
            else
                Toast.makeText(applicationContext, "LOCATION PERMISSIONS DENIED!", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Método para asignar todos los eventos de la aplicación.
     */
    private fun setEvents() {
        map!!.setOnMyLocationButtonClickListener(this)
        map!!.setOnMyLocationClickListener(this)
    }

    /**
     * Método para obtener una coordenadas a través de una dirección.
     * @param direccion String con la dirección a buscar.
     * Mediante el uso del atributo geoCoder, transformamos la dirección escrita en unas coordenadas. El método
     * getFromLocationName() nos devuelve una lista de Address con todos los posibles resultados. Presuponemos
     * que el objeto Address de la primera posición almacenará la que buscamos, siendo esta la que transformamos
     * en unas coordenadas para ser mostradas en el mapa.
     */
    @Throws(IOException::class)
    private fun getCoordinates(direccion: String): LatLng? {
        var coordenadas: LatLng? = null
        val listLocalizaciones = geocoder!!.getFromLocationName(direccion, 1)

        if (listLocalizaciones.size > 0) {
            val localizacion = listLocalizaciones[0]
            val latitud = localizacion.latitude
            val longitud = localizacion.longitude

            coordenadas = LatLng(latitud, longitud)
        } else
            Toast.makeText(this, "Dirección no encontrada", Toast.LENGTH_SHORT).show()

        return coordenadas
    }

    /**
     * Método que muestra una dirección en el mapa.
     * @param direccion Dirección a buscar.
     * Obtenemos las coordenadas mediante el método getCoordinates. Si las coordenadas no son iguales a null,
     * creamos una instancia de tipo CameraUpdate y se la pasamos al método moveCamera() para realizar un movimiento
     * en el mapa de la aplicación, mostrándose en el centro la ubicación buscada.
     */
    @Throws(IOException::class)
    private fun searchAddress(direccion: String) {
        val coordinates = getCoordinates(direccion)

        if (coordinates != null) {
            wantedAddress = coordinates
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(coordinates, 17f)

            map!!.moveCamera(cameraUpdate)
            hideKeyboard()
        }
    }

    /**
     * Método para ocultar el teclado virtual de la pantalla.
     */
    private fun hideKeyboard() {
        val view = this.currentFocus

        if (view != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager!!.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map!!.setOnMarkerClickListener(this)

        setEvents()
        moveMap()
    }

    @SuppressLint("MissingPermission")
    private fun moveMap() {
        map!!.isMyLocationEnabled = true

        locationProvider!!.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                val currentLatLng = LatLng(location.latitude, location.longitude)

                map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }

    override fun onMyLocationClick(location: Location) {
        val mensaje = "Localización actual: " + location.latitude + ", " + location.longitude

        println(mensaje)
    }

    override fun onKey(view: View, keyCode: Int, event: KeyEvent): Boolean {
        if (view.id == editTextAddress!!.id) {
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                val address = editTextAddress!!.text.toString()

                if (address != EMPTY) {
                    try {
                        searchAddress(address)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
                return true
            }
        }
        return false
    }

    private fun requestLocationPermissions() {
        val permisos = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, permisos[0]) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permisos, REQUEST_LOCATION_PERMISSIONS_CODE)
            } else {
                startApplication()
            }
        } else {
            startApplication()
        }
    }

    private fun checkCompanyProximity(currentPosition: LatLng) {
        offersController.companies.forEach { company ->
            val companyLocation = company.position
            val distanceResult = FloatArray(2)
            val circle = map!!.addCircle(CircleOptions()
                    .center(companyLocation)
                    .radius(50.toDouble()))
            circle.isVisible = false

            Location.distanceBetween(currentPosition.latitude,
                    currentPosition.longitude,
                    circle.center.latitude,
                    circle.center.longitude,
                    distanceResult)

            if(distanceResult[0] < circle.radius) {
                println("ESTÄ EN CHEKCPROXIMITY")
                markers.forEach { marker ->
                    if(marker.position == companyLocation) {
                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(offersController.resizeMapIcons("ic_grey_marker", applicationContext)))
                        println("AHORA DEBERIA ESTAR GRIS")
                    }
                }
            }
        }
    }

    override fun onSuccess(location: Location?) {
        if (location != null)
            currentLocation = location
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.my_offers_menu, menu)
        menuInflater.inflate(R.menu.refresh_action, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.my_offers -> {
                val intent = Intent(applicationContext, MyOffersActivity::class.java)

                intent.putExtra(Utils.ID, currentUserId)
                startActivity(intent)
            }

            R.id.action_refresh -> initCloudDownloads()

            R.id.close_filter -> {
                changeToolbarMenuIcons(false)
                refreshMapOffers()
            }

            R.id.logout -> finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        offersController.companies.forEach { company ->
            if(marker == company.marker)
                showOfferInfo(company)
        }
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun showOfferInfo(company: Company, offerIndex: Int = -1) {
        val layoutView = View.inflate(applicationContext, R.layout.marker_infowindow, null)
        val companyName = layoutView.findViewById<TextView>(R.id.company_name_textview)
        val offerType = layoutView.findViewById<TextView>(R.id.offer_type_textview)
        val salary = layoutView.findViewById<TextView>(R.id.offer_salary_textview)
        val experience = layoutView.findViewById<TextView>(R.id.offer_experience_textview)
        val companyOffers = ArrayList<Offer>()
        val positiveButtonText: String
        val companyNameText: String
        var index = offerIndex

        offersController.offers.forEach { offer ->
            if(offer.companyId == company.id)
                companyOffers.add(offer)
        }

        if(index == -1)
            index = 0

        companyNameText = if(companyOffers.size == 1)
            company.name
        else
            "${company.name}  ${index + 1}/${companyOffers.size}"

        companyName.text = companyNameText
        offerType.text = companyOffers[index].type
        experience.text = "Experience: ${companyOffers[index].experience}"
        salary.text = "Salary: ${companyOffers[index].salary}"

        val builder = AlertDialog.Builder(this)

        positiveButtonText = if(companyOffers.size > 1)
            "Next"
        else
            "Ok"

        builder.setView(layoutView)
                .setCancelable(true)
                .setPositiveButton(positiveButtonText) { _, _ ->
                    if(companyOffers.size > 1) {
                        if((index + 1) == companyOffers.size)
                            index = 0
                        else
                            index += 1

                        showOfferInfo(company, index)
                    }
                }

        if(index > 0)
            builder.setNegativeButton("Previous") {_, _ ->
                showOfferInfo(company, --index)
            }

        builder.setCancelable(true)
        builder.show()
    }

    @SuppressLint("SetTextI18n")
    private fun showOfferFilterDialog() {
        val layoutView = View.inflate(applicationContext, R.layout.filter_offers_layout, null)
        val builder = AlertDialog.Builder(this)
        layoutView.findViewById<TextView>(R.id.header_add_offer).text = "Filter offers:"

        val experienceRadioGroup = layoutView.findViewById<RadioGroup>(R.id.experience_radioGroup)
        val salaryRadioGroup = layoutView.findViewById<RadioGroup>(R.id.salary_radioGroup)
        val typeOfferEditText = layoutView.findViewById<EditText>(R.id.type_offer_editText)

        builder.setView(layoutView)
                .setCancelable(true)
                .setPositiveButton("Filter") { _, _ ->
                    changeToolbarMenuIcons(true)
                    val idRadioButtonExperience = experienceRadioGroup.checkedRadioButtonId
                    val idRadioButtonSalary = salaryRadioGroup.checkedRadioButtonId

                    val type = typeOfferEditText.text.toString().toUpperCase()
                    var experienceSelected = experienceRadioGroup.findViewById<RadioButton>(idRadioButtonExperience).text.toString()
                    if(experienceSelected == "not defined")
                        experienceSelected = ""
                    var salarySelected = salaryRadioGroup.findViewById<RadioButton>(idRadioButtonSalary).text.toString()
                    if(salarySelected == "not defined")
                        salarySelected = ""
                    val offer = Offer(-1, type, salarySelected, experienceSelected, -1)

                    refreshMapOffers(offer)
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }

        builder.setCancelable(true)
        builder.show()
    }

    private fun changeToolbarMenuIcons(isFiltering: Boolean) {
        menu!!.clear()

        if(isFiltering)
            menuInflater.inflate(R.menu.close_filter_action, menu)
        else {
            menuInflater.inflate(R.menu.my_offers_menu, menu)
            menuInflater.inflate(R.menu.refresh_action, menu)
        }
    }

    private fun refreshMapOffers(offer: Offer? = null) {
        if(offer == null)
            offersController.getOffers()
        else
            offersController.getOffers(offer)

        markers = offersController.getCompaniesMarkers(map!!, applicationContext)
    }

    override fun processFinish(response: String) {
        refreshMapOffers()
    }

    override fun processFailed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
