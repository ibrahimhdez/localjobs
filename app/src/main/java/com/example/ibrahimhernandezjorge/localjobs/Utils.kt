package com.example.ibrahimhernandezjorge.localjobs

object Utils {
    const val TABLA_EMPRESAS = "EMPRESAS"
    const val TABLA_OFERTAS = "OFERTAS"
    const val TABLA_ESTADO_OFERTAS = "ESTADO_OFERTAS"
    const val TABLA_USUARIOS = "USUARIOS"

    const val ID = "ID"
    const val ID_EMPRESA = "ID_EMPRESA"
    const val ID_USUARIO = "ID_USUARIO"
    const val ID_OFERTA = "ID_OFERTA"

    const val TYPE = "TIPO"
    const val SALARY = "SUELDO"
    const val EXPERIENCE = "EXPERCIENCIA"
    const val NAME = "NOMBRE"
    const val ADDRESS = "DIRECCION"
    const val LATITUD = "LATITUD"
    const val LONGITUD = "LONGITUD"
    const val STATE = "ESTADO"
    const val USERNAME = "USERNAME"
    const val SURNAME = "APELLIDOS"

    const val SELECTED = "SELECCIONADO"
    const val DISCARD = "DESCARTADO"
    const val INSCRIT = "INSCRITO"
}