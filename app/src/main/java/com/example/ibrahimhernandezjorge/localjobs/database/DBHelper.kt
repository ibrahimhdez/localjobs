package com.example.ibrahimhernandezjorge.localjobs.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.icu.util.UniversalTimeScale
import com.example.ibrahimhernandezjorge.localjobs.Utils
import org.json.JSONArray
import org.json.JSONObject

class DBHelper internal constructor(context : Context) : SQLiteOpenHelper(context, "database", null, 1) {
    companion object {
        //Constante que contiene la sentencia SQL de creación de la tabla USUARIOS
        const val SQL_CREATE_USUARIOS =
                "CREATE TABLE ${Utils.TABLA_USUARIOS} (" +
                        "${Utils.ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "${Utils.USERNAME} TEXT(30), " +
                        "${Utils.NAME} TEXT(30), " +
                        "${Utils.SURNAME} TEXT(30), " +
                        "${Utils.STATE} TEXT(30)" +
                        ");"
        //Constante que contiene la sentencia SQL de creación de la tabla EMPRESAS
        const val SQL_CREATE_EMPRESAS =
                "CREATE TABLE " + Utils.TABLA_EMPRESAS + "(" +
                        Utils.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Utils.NAME + " TEXT(30), " +
                        Utils.ADDRESS + " TEXT(150), " +
                        Utils.LATITUD + " DOUBLE, " +
                        Utils.LONGITUD + " DOUBLE, " +
                        Utils.TYPE + " TEXT(30) " +
                        ");"

        //Constante que contiene la sentencia SQL de creación de la tabla OFERTAS
        const val SQL_CREATE_OFERTAS =
                "CREATE TABLE " + Utils.TABLA_OFERTAS + "(" +
                        Utils.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Utils.TYPE + " TEXT(30), " +
                        Utils.SALARY + " TEXT(20), " +
                        Utils.EXPERIENCE + " TEXT(15), " +
                        Utils.ID_EMPRESA + " INTEGER, " +
                        "FOREIGN KEY(${Utils.ID_EMPRESA}) REFERENCES ${Utils.TABLA_EMPRESAS}(${Utils.ID}) ON DELETE CASCADE" +
                        ");"

        //Constante que contiene la sentencia SQL de creación de la tabla ESTADO_OFERTAS
        const val SQL_CREATE_ESTADO_OFERTAS =
                "CREATE TABLE ${Utils.TABLA_ESTADO_OFERTAS}(" +
                        "${Utils.ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "${Utils.STATE} TEXT(30), " +
                        "${Utils.ID_USUARIO} INT, " +
                        "${Utils.ID_OFERTA} INT, " +
                        "FOREIGN KEY(${Utils.ID_OFERTA}) REFERENCES ${Utils.TABLA_OFERTAS}(${Utils.ID}) ON DELETE CASCADE" +
                        ");"
    }

    /**
     * Método que se inicia en primer momento. Crea todas las tablas
     * @param db Objeto SQLiteDatabase
     */
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_USUARIOS)
        db?.execSQL(SQL_CREATE_EMPRESAS)
        db?.execSQL(SQL_CREATE_OFERTAS)
        db?.execSQL((SQL_CREATE_ESTADO_OFERTAS))
    }

    /**
     * Método que se ejecuta cuando se actualiza la base de datos
     * @param db Objeto SQLiteDatabase
     * @param p1 versión anterior
     * @param p2 nueva versión
     */
    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        destroy(db)
        onCreate(db)
    }

    /**
     * Método que destruye las tablas de la base de datos
     * @param db Writable de la base de datos
     */
    private fun destroy(db: SQLiteDatabase?) {
        db?.execSQL("DROP TABLE IF EXISTS ${Utils.TABLA_USUARIOS}")
        db?.execSQL("DROP TABLE IF EXISTS ${Utils.TABLA_EMPRESAS}")
        db?.execSQL("DROP TABLE IF EXISTS ${Utils.TABLA_OFERTAS}")
        db?.execSQL("DROP TABLE IF EXISTS ${Utils.TABLA_ESTADO_OFERTAS}")
    }

    fun insertUser(json: JSONObject, state: String) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        //TODO Probar insertar todo en una transacción
        bd.beginTransaction() //Comienza la transacción

        registro.put(Utils.ID, json.getInt(Utils.ID))
        registro.put(Utils.USERNAME, json.getString(Utils.USERNAME))
        registro.put(Utils.NAME, json.getString(Utils.NAME))
        registro.put(Utils.SURNAME, json.getString(Utils.SURNAME))
        registro.put(Utils.STATE, state)

        //Inserta el registro en la base de datos
        bd.insert(Utils.TABLA_USUARIOS, null, registro)
        closeTransaction(bd)
    }

    fun insertCompany(json: JSONObject) {
        println(json)
        val bd = this.writableDatabase
        val registro = ContentValues()

        bd.beginTransaction()

        registro.put(Utils.ID, json.getInt(Utils.ID))
        registro.put(Utils.NAME, json.getString(Utils.NAME))
        registro.put(Utils.ADDRESS, json.getString(Utils.ADDRESS))
        registro.put(Utils.LATITUD, json.getDouble(Utils.LATITUD))
        registro.put(Utils.LONGITUD, json.getDouble(Utils.LONGITUD))
        registro.put(Utils.TYPE, json.getString(Utils.TYPE))

        bd.insert(Utils.TABLA_EMPRESAS, null, registro)
        closeTransaction(bd)
    }

    fun insertOffers(jsonArray: JSONArray) {
        for(i in 0 until jsonArray.length()) {
            val json = jsonArray[i] as JSONObject

            val bd = this.writableDatabase
            val registro = ContentValues()

            //TODO Probar insertar todo en una transacción
            bd.beginTransaction() //Comienza la transacción

            registro.put(Utils.ID, json.getInt(Utils.ID))
            registro.put(Utils.TYPE, json.getString(Utils.TYPE))
            registro.put(Utils.SALARY, json.getString(Utils.SALARY))
            registro.put(Utils.EXPERIENCE, json.getString(Utils.EXPERIENCE))
            registro.put(Utils.ID_EMPRESA, json.getInt(Utils.ID_EMPRESA))

            //Inserta el registro en la base de datos
            bd.insert(Utils.TABLA_OFERTAS, null, registro)
            closeTransaction(bd)
        }
    }

    fun insertOffersStates(jsonArray: JSONArray) {
        for(i in 0 until jsonArray.length()) {
            val json = jsonArray[i] as JSONObject

            val bd = this.writableDatabase
            val registro = ContentValues()

            //TODO Probar insertar todo en una transacción
            bd.beginTransaction() //Comienza la transacción

            registro.put(Utils.ID, json.getInt(Utils.ID))
            registro.put(Utils.STATE, json.getString(Utils.STATE))
            registro.put(Utils.ID_USUARIO, json.getInt(Utils.ID_USUARIO))
            registro.put(Utils.ID_OFERTA, json.getInt(Utils.ID_OFERTA))

            //Inserta el registro en la base de datos
            bd.insert(Utils.TABLA_ESTADO_OFERTAS, null, registro)
            closeTransaction(bd)
        }
    }

    fun deleteOffer(offerId: Int) {
        val bd = this.writableDatabase

        bd.execSQL("PRAGMA foreign_keys=ON;")
        bd.beginTransaction()
        bd.delete(Utils.TABLA_OFERTAS, "${Utils.ID} = $offerId", null)
        closeTransaction(bd)
    }


    fun deleteUsers() {
        val bd = this.writableDatabase

        bd.execSQL("PRAGMA foreign_keys=ON;")
        bd.beginTransaction()
        bd.execSQL("DELETE FROM ${Utils.TABLA_USUARIOS};")
        closeTransaction(bd)
    }

    fun changeOfferState(offerId: Int, userId: Int, state: String) {
        val bd = this.writableDatabase

        bd.beginTransaction()
        bd.execSQL("UPDATE ${Utils.TABLA_USUARIOS} SET ${Utils.STATE} = '$state' " +
                "WHERE ${Utils.ID} = $userId;\n")
        closeTransaction(bd)
    }

    /**
     * Método que cierra y confirma una transacción
     * @param bd Objeto base de datos con permisos de escritura en la que será confirmada la transacción
     */
    private fun closeTransaction(bd: SQLiteDatabase) {
        bd.setTransactionSuccessful() //Confirmar la transacción para que los cambios sean guardados
        bd.endTransaction() //Finalizar la transacción
        bd.close() //Cerrar base de datos
    }

    /**
     * Método para realizar una consulta personalizada
     * @return Cursor con el resultado de la consulta
     */
    private fun cursorConsultaPersonalizada(tabla: String, tableColumns: Array<String>?, whereClause: String?, whereArgs: Array<String>?,
                                            groupBy: String?, having: String?, orderBy: String?, limite: Int? = null): Cursor {
        return this.readableDatabase.query(tabla, tableColumns, whereClause, whereArgs, groupBy, having, orderBy, limite?.toString())
    }

    /**
     * Método para realizar consulta
     * @return Cursor con el resultado de la consulta
     */
    fun cursorConsulta(tabla: String, atributos: Array<String>?, valores: Array<String>?, limite: Int? = null): Cursor {
        return if (atributos != null) {
            var whereClause = atributos[0] + "= ?"
            for (i in 1 until atributos.size)
                whereClause += " AND " + atributos[i] + "= ?"
            cursorConsultaPersonalizada(tabla, null, whereClause, valores, null, null, null, limite)
        } else
            cursorConsultaPersonalizada(tabla, null, null, null, null, null, null, limite)
    }

    /**
     * Método que devuelve una tabla formateada a String para mostrar
     * @return String con el resultado de mostrar la tabla
     */
    fun  getTableAsString(tableName: String): String {
        val db = this.readableDatabase
        var tableString = String.format("Table %s:\n", tableName)
        val allRows = db.rawQuery("SELECT * FROM $tableName", null)
        if (allRows.moveToFirst()) {
            val columnNames = allRows.columnNames
            do {
                for (name in columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)))
                }
                tableString += "\n"

            } while (allRows.moveToNext())
        }
        allRows.close()
        return tableString
    }
}