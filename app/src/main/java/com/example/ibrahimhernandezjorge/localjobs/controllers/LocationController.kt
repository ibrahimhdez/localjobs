package com.example.ibrahimhernandezjorge.localjobs.controllers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.support.v4.content.ContextCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnSuccessListener
import java.io.IOException

class LocationController(activity: Activity, var sendPositionFun: ((LatLng) -> Unit)? = null) : OnSuccessListener<Location> {
    var locationProvider: FusedLocationProviderClient? = null
    var locationRequest: LocationRequest? = null
    var locationCallback: LocationCallback? = null
    var geocoder: Geocoder? = null
    var currentLocation: Location? = null

    companion object {
        private val LOCATION_INTERVAL = 5000
        private val FASTEST_INTERVAL_LOCALIZACION = 1000
    }

    init {
        locationProvider = LocationServices.getFusedLocationProviderClient(activity)
        geocoder = Geocoder(activity)
    }

    /**
     * Método para iniciar la recepción de las localizaciones.
     * Debemos comprobar antes que se han concedido los permisos (es necesario hacerlo siempre).
     */
    fun initLocationReception(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationProvider!!.requestLocationUpdates(locationRequest, locationCallback!!, null)
            }
        }
    }

    /**
     * Método para crear nuestra solicitud de localización, estableciendo todos los parámetros.
     */
    fun createLocationRequest() {
        locationRequest = LocationRequest.create()

        locationRequest!!.interval = LOCATION_INTERVAL.toLong()
        locationRequest!!.fastestInterval = FASTEST_INTERVAL_LOCALIZACION.toLong()
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Método para inicializar el atributo CallbackLocalizacion.
     * Creamos una nueva LocationCallback, en la que describimos que hacer con los resultados de la solicitud
     * de localización. Almacenamos la dirección devuelta y mostramos los datos en pantalla.
     */
    fun initLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult != null) {
                    for (location in locationResult.locations) {
                        try {
                            currentLocation = location
                            showLocationData()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                }
            }
        }
    }

    /**
     * Función que obtiene la dirección a partir de las coordenadas almacenadas en el atributo localizacionActual.
     */
    private fun getAddress(): String? {
        val locationList = geocoder!!.getFromLocation(currentLocation!!.latitude, currentLocation!!.longitude, 1)
        var direccion: String? = null

        if (locationList.size > 0) {
            val localizacion = locationList[0]
            direccion = localizacion.getAddressLine(0)
        } else {
          //  Toast.makeText(this, "Dirección no encontrada", Toast.LENGTH_SHORT).show()
        }
        return direccion
    }

    fun getCoordinates(address: String): LatLng? {
        val addresses = geocoder!!.getFromLocationName(address, 1)
        var position: LatLng? = null

        if(addresses.size > 0) {
            val latitude = addresses[0].latitude
            val longitude = addresses[0].longitude

            position = LatLng(latitude, longitude)
        }

        return position
    }

    /**
     * Método para mostrar en pantalla los datos de la localización.
     */
    private fun showLocationData() {
        val latitud = currentLocation!!.latitude
        val longitud = currentLocation!!.longitude
        val direccion = getAddress()
        val position = LatLng(latitud, longitud)

        sendPositionFun!!.invoke(position)
    }

    /**
     * Método onSuccess llamado cuando los permisos han sido concedidos y queremos obtener la última localización
     * conocida del dispositivo.
     * @param location
     */
    override fun onSuccess(location: Location?) {
        if (location != null) {
            currentLocation = location
            try {
                showLocationData()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }
}