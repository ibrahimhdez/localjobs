package com.example.ibrahimhernandezjorge.localjobs.activities

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.ibrahimhernandezjorge.localjobs.controllers.CloudController
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.database.DBComunicator
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.login_activity.*

class LoginActivity : AppCompatActivity() {
    private var cloud = CloudController
    private lateinit var spotsDialog: AlertDialog
    private lateinit var usernameEditText: EditText
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button
    private lateinit var companyTextView: TextView
    private lateinit var companyEditText: EditText
    private lateinit var companyButton: Button
    private lateinit var registerCompany: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        initComponentes()
    }

    private fun initComponentes() {
        DBComunicator.initInstance(applicationContext)
        spotsDialog = SpotsDialog.Builder()
                .setContext(this)
                .setTheme(R.style.spotsDialogStyle)
                .build()
        usernameEditText = findViewById(R.id.username_edittext)
        loginButton = findViewById(R.id.login_button)
        registerButton = findViewById(R.id.to_register_button)
        companyTextView = findViewById(R.id.company_login_textView)
        companyEditText = findViewById(R.id.company_name_editText)
        companyButton = findViewById(R.id.companylogin_button)
        registerCompany = findViewById(R.id.register_company_button)

        initEvents()
    }

    private fun initEvents() {
        loginButton.setOnClickListener {
            spotsDialog.show()
            val username = usernameEditText.text.toString().toUpperCase()

            if(username.isNotEmpty())
                cloud.login(username, ::logging)
        }

        registerButton.setOnClickListener {
            val intent = Intent(applicationContext, RegisterActivity::class.java)

            intent.putExtra(Utils.TYPE, Utils.TABLA_USUARIOS)
            startActivity(intent)
        }

        companyTextView.setOnClickListener {
            company_name_textInput.visibility = View.VISIBLE
            companyButton.visibility = View.VISIBLE
            registerCompany.visibility = View.VISIBLE
        }

        companyButton.setOnClickListener {
            spotsDialog.show()
            val companyName = companyEditText.text.toString().toUpperCase()

            if(companyName.isNotEmpty())
                cloud.companyLogin(companyName, ::companyLoggin)
        }

        registerCompany.setOnClickListener {
            val intent = Intent(applicationContext, RegisterActivity::class.java)

            intent.putExtra(Utils.TYPE, Utils.TABLA_EMPRESAS)
            startActivity(intent)
        }
    }

    private fun logging(userId: Int) {
        spotsDialog.dismiss()
        if(userId > -1) {
            val intent = Intent(applicationContext, MapsActivity::class.java)

            intent.putExtra(Utils.ID, userId)
            startActivity(intent)
        }
        else
            Toast.makeText(applicationContext, "No register user!", Toast.LENGTH_LONG).show()
    }

    private fun companyLoggin(companyId: Int) {
        spotsDialog.dismiss()
        if(companyId > -1) {
            val intent = Intent(applicationContext, CompanyActivity::class.java)

            intent.putExtra(Utils.ID_EMPRESA, companyId)
            startActivity(intent)
        }
    }
}
