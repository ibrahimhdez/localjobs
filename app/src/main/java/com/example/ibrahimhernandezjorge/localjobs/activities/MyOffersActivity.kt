package com.example.ibrahimhernandezjorge.localjobs.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.ibrahimhernandezjorge.localjobs.controllers.CloudController
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.adapters.MyOffersAdapter
import com.example.ibrahimhernandezjorge.localjobs.database.DBComunicator
import com.example.ibrahimhernandezjorge.localjobs.utils.StateOffers

class MyOffersActivity : AppCompatActivity() {
    private var cloud = CloudController
    private var recycler: RecyclerView? = null
    private var adapter: MyOffersAdapter? = null
    private var currentUserId = -1
    private var myOffers = ArrayList<StateOffers>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_offers_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initComponentes()
    }

    private fun initComponentes() {
        currentUserId = intent.extras!!.getInt(Utils.ID)
        recycler = findViewById(R.id.my_offers_recycler)
        recycler!!.layoutManager = LinearLayoutManager(this)

        refreshRecycler()
    }

    fun refreshRecycler(isRefreshing: Boolean = false) {
        initAdapter()
        recycler!!.adapter = adapter

        if(isRefreshing)
            Toast.makeText(applicationContext, "Updated!", Toast.LENGTH_LONG).show()
    }

    private fun initAdapter() {
        myOffers = getMyOffers()
        adapter = MyOffersAdapter(myOffers)
    }

    private fun getMyOffers(): ArrayList<StateOffers> {
        val offers = ArrayList<StateOffers>()
        val stateOffersCursor = DBComunicator.getInstance().cursorConsulta(Utils.TABLA_ESTADO_OFERTAS, arrayOf(Utils.ID_USUARIO), arrayOf(currentUserId.toString()))

        while(stateOffersCursor.moveToNext()) {
            val offerId = stateOffersCursor.getInt(stateOffersCursor.getColumnIndex(Utils.ID_OFERTA))
            val offerCursor = DBComunicator.getInstance().cursorConsulta(Utils.TABLA_OFERTAS, arrayOf(Utils.ID), arrayOf(offerId.toString()))
            offerCursor.moveToFirst()

            val companyId = offerCursor.getInt(offerCursor.getColumnIndex(Utils.ID_EMPRESA))
            val companyCursor = DBComunicator.getInstance().cursorConsulta(Utils.TABLA_EMPRESAS, arrayOf(Utils.ID), arrayOf(companyId.toString()))
            companyCursor.moveToFirst()

            val type =  offerCursor.getString(offerCursor.getColumnIndex(Utils.TYPE))
            val state = stateOffersCursor.getString(stateOffersCursor.getColumnIndex(Utils.STATE))
            val companyName = companyCursor.getString(companyCursor.getColumnIndex(Utils.NAME))
            val myOffer = StateOffers(type, companyName, state)

            offers.add(myOffer)
        }

        return offers
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.refresh_action, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.action_refresh -> {
                cloud.getUserOffers(currentUserId, ::refreshRecycler)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
}