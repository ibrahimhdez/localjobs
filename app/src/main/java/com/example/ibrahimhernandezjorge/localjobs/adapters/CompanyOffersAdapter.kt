package com.example.ibrahimhernandezjorge.localjobs.adapters

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.utils.CompanyOffers

class CompanyOffersAdapter(offers: ArrayList<CompanyOffers>, offerMenu: (CardView, Int) -> Unit): RecyclerView.Adapter<CompanyOffersAdapter.ViewHolderOffer>() {
    private var companyOffers = offers
    private var offerMenuFun = offerMenu
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOffer {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_company_offer, parent, false)

        context = view.context

        return ViewHolderOffer(view)
    }

    override fun onBindViewHolder(holder: ViewHolderOffer, position: Int) {
        holder.type.text = companyOffers[position].type
        holder.salary.text = companyOffers[position].salary
        holder.experience.text = companyOffers[position].experience
        offerMenuFun.invoke(holder.card, position)
    }

    override fun getItemCount(): Int {
        return companyOffers.size
    }

    inner class ViewHolderOffer(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val card: CardView = itemView.findViewById(R.id.card_company_offer)
        val type: TextView = itemView.findViewById(R.id.name_userOfOffer)
        val salary: TextView = itemView.findViewById(R.id.salary_textView)
        val experience: TextView = itemView.findViewById(R.id.experience_textView)
    }
}
