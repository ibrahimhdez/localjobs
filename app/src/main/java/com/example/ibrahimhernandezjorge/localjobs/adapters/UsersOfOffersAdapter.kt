package com.example.ibrahimhernandezjorge.localjobs.adapters

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.utils.User

class UsersOfOffersAdapter(usersArray: ArrayList<User>, showStateDialog: (Int) -> Unit): RecyclerView.Adapter<UsersOfOffersAdapter.ViewHolderOffer>() {
    private var users = usersArray
    private var showStateDialogFun = showStateDialog
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOffer {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_users_of_offers, parent, false)

        context = view.context

        return ViewHolderOffer(view)
    }

    override fun onBindViewHolder(holder: ViewHolderOffer, position: Int) {
        holder.username.text = users[position].username

        holder.card.setOnClickListener {
            showStateDialogFun.invoke(users[position].id)
        }

        setOfferIcon(holder.stateIcon, users[position].state)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    private fun setOfferIcon(icon: ImageView, state: String) {
        when(state) {
            Utils.INSCRIT -> {
                val imageDrawable = context!!.resources.getDrawable(R.drawable.ic_inscrit)

                icon.setImageDrawable(imageDrawable)
            }

            Utils.SELECTED -> {
                val imageDrawable = context!!.resources.getDrawable(R.drawable.ic_ok)

                icon.setImageDrawable(imageDrawable)
            }

            Utils.DISCARD -> {
                val imageDrawable = context!!.resources.getDrawable(R.drawable.ic_discard)

                icon.setImageDrawable(imageDrawable)
            }

        }
    }

    inner class ViewHolderOffer(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var card: CardView = itemView.findViewById(R.id.card_userOfOffers)
        var username: TextView = itemView.findViewById(R.id.name_userOfOffer)
        var stateIcon: ImageView = itemView.findViewById(R.id.state_icon_userOfOffers)
    }
}
