package com.example.ibrahimhernandezjorge.localjobs.controllers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.database.DBComunicator
import com.example.ibrahimhernandezjorge.localjobs.utils.Company
import com.example.ibrahimhernandezjorge.localjobs.utils.Offer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class OffersController {
    var companies = ArrayList<Company>()
    var offers = ArrayList<Offer>()

    fun getCompaniesMarkers(map: GoogleMap, context: Context): ArrayList<Marker> {
        companies.clear()
        map.clear()

        val markers = ArrayList<Marker>()
        val cursor = DBComunicator.getInstance().writableDatabase.rawQuery("SELECT * FROM ${Utils.TABLA_EMPRESAS};", null)

        while(cursor.moveToNext()) {
            val id = cursor.getInt(cursor.getColumnIndex(Utils.ID))

            if(offers.any { offer -> offer.companyId == id }) {
                val name = cursor.getString(cursor.getColumnIndex(Utils.NAME))
                val latitud = cursor.getDouble(cursor.getColumnIndex(Utils.LATITUD))
                val longitud = cursor.getDouble(cursor.getColumnIndex(Utils.LONGITUD))
                val position = LatLng(latitud, longitud)
                val markerOptions = MarkerOptions()
                        .position(position)
                        .title("")
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("blue_marker", context)))

                val marker = map.addMarker(markerOptions)
                marker.setInfoWindowAnchor(500F, 500F)
                val company = Company(id, name, position, marker)

                markers.add(marker)
                companies.add(company)
            }
        }
        cursor.close()

        return markers
    }

    fun getOffers(filterOffer: Offer? = null) {
        offers.clear()
        var queryArguments = ""
        var previousArgument = false

        if(filterOffer != null) {
            queryArguments = "WHERE "

            if (filterOffer.type.isNotEmpty()) {
                previousArgument = true
                queryArguments += "${Utils.TYPE} = '${filterOffer.type}' "
            }

            if(filterOffer.experience.isNotEmpty()) {
                if(previousArgument)
                    queryArguments += "AND "
                else
                    previousArgument = true
                queryArguments += "${Utils.EXPERIENCE} = '${filterOffer.experience}' "
            }

            if(filterOffer.salary.isNotEmpty()) {
                if(previousArgument)
                    queryArguments += "AND "
                queryArguments += "${Utils.SALARY} = '${filterOffer.salary}' "
            }

        }

        val cursor = DBComunicator.getInstance().writableDatabase.rawQuery("SELECT * FROM ${Utils.TABLA_OFERTAS} $queryArguments;", null)

        while(cursor.moveToNext()) {
            val id = cursor.getInt(cursor.getColumnIndex(Utils.ID))
            val type = cursor.getString(cursor.getColumnIndex(Utils.TYPE))
            val salary = cursor.getString(cursor.getColumnIndex(Utils.SALARY))
            val experience = cursor.getString(cursor.getColumnIndex(Utils.EXPERIENCE))
            val companyId = cursor.getInt(cursor.getColumnIndex(Utils.ID_EMPRESA))
            val offer = Offer(id, type, salary, experience, companyId)

            offers.add(offer)
        }

        cursor.close()
    }

    fun resizeMapIcons(iconName: String, context: Context): Bitmap {
        val imageBitmap = BitmapFactory.decodeResource(context.resources, context.resources.getIdentifier(iconName, "drawable", context.packageName))

        return Bitmap.createScaledBitmap(imageBitmap, 150, 150, false)
    }
}