package com.example.ibrahimhernandezjorge.localjobs.activities

import android.app.AlertDialog
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import com.example.ibrahimhernandezjorge.localjobs.controllers.CloudController
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.adapters.UsersOfOffersAdapter
import com.example.ibrahimhernandezjorge.localjobs.asyncTasks.AsyncResponse
import com.example.ibrahimhernandezjorge.localjobs.database.DBComunicator
import com.example.ibrahimhernandezjorge.localjobs.utils.User

class UsersOfOffersActivity: AppCompatActivity(), AsyncResponse {
    private var cloud = CloudController
    private var recycler: RecyclerView? = null
    private lateinit var adapter: UsersOfOffersAdapter
    private var users = ArrayList<User>()
    private var offerId = -1
    private var radioButtonArray = ArrayList<RadioButton>()
    private lateinit var selectedStateCheckBox: RadioButton
    private lateinit var discardStateCheckBox: RadioButton
    private var changingState = false
    private lateinit var floatingActionButton: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_of_offers)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initComponentes()
        startOfferDownload()
    }

    private fun initComponentes() {
        floatingActionButton = findViewById(R.id.fab_add_user)
        floatingActionButton.setOnClickListener {
            showAddUserDialog()
        }

        offerId = intent.extras!!.getInt(Utils.ID)
        recycler = findViewById(R.id.recycler_users_of_offers)
        recycler!!.layoutManager = LinearLayoutManager(this)
    }

    private fun startOfferDownload() {
        DBComunicator.getInstance().deleteUsers()
        cloud.getUsersOfAOffer(offerId, this)
    }

    private fun refreshAdapter() {
        users = getUsers()
        adapter = UsersOfOffersAdapter(users, ::showStateChangerDialog)
        recycler!!.adapter = adapter

        adapter.notifyDataSetChanged()
    }

    private fun getUsers(): ArrayList<User> {
        val users = ArrayList<User>()
        val cursor = DBComunicator.getInstance().writableDatabase.rawQuery("SELECT * FROM ${Utils.TABLA_USUARIOS}", null)

        while(cursor.moveToNext()) {
            val id = cursor.getInt(cursor.getColumnIndex(Utils.ID))
            val username = cursor.getString(cursor.getColumnIndex(Utils.USERNAME))
            val state = cursor.getString(cursor.getColumnIndex(Utils.STATE))
            val user = User(id = id, username = username, name = "", surname = "", state = state)

            users.add(user)
        }
        cursor.close()

        return users
    }

    private fun showStateChangerDialog(userId: Int) {
        val radioButtonsView = View.inflate(applicationContext, R.layout.dialog_set_offer_state, null)
        selectedStateCheckBox = radioButtonsView.findViewById(R.id.selected_state_radiobutton)
        discardStateCheckBox = radioButtonsView.findViewById(R.id.discard_state_radiobutton)

        radioButtonArray.add(selectedStateCheckBox)
        radioButtonArray.add(discardStateCheckBox)

        val builder = AlertDialog.Builder(this)
        builder.setView(radioButtonsView)
                .setCancelable(true)
                .setPositiveButton("Ok") { _, _ ->
                    println(DBComunicator.getInstance().getTableAsString(Utils.TABLA_ESTADO_OFERTAS))
                    changingState = true
                    var state = ""

                    radioButtonArray.forEach { radioButton ->
                        if(radioButton.isChecked)
                            state = radioButton.text.toString().toUpperCase()
                    }

                    state = changeLanguage(state)
                    cloud.changeOfferState(offerId, userId, state, this)

                }.setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
        builder.show()
    }

    private fun showAddUserDialog() {
        val layoutView = View.inflate(applicationContext, R.layout.dialog_add_user_layout, null)
        val usernameEditText = layoutView.findViewById<EditText>(R.id.username_edittext)

        val builder = AlertDialog.Builder(this)
        builder.setView(layoutView)
                .setCancelable(true)
                .setPositiveButton("Add") { _, _ ->
                    val username = usernameEditText.text.toString().toUpperCase()

                    cloud.addUserToAOffer(offerId, username)

                }.setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
        builder.show()
    }

    private fun changeLanguage(state: String): String {
        return if(state == "SELECTED")
            "SELECCIONADO"
        else
            "DESCARTADO"
    }

    override fun processFinish(response: String) {
        refreshAdapter()
    }

    override fun processFailed() {
        Toast.makeText(applicationContext, "CONNECTION ERROR!", Toast.LENGTH_LONG).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
}