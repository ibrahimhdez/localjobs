package com.example.ibrahimhernandezjorge.localjobs.controllers

import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.asyncTasks.AsyncResponse
import com.example.ibrahimhernandezjorge.localjobs.asyncTasks.AsyncTask
import com.example.ibrahimhernandezjorge.localjobs.database.DBComunicator
import com.example.ibrahimhernandezjorge.localjobs.utils.Offer
import com.example.ibrahimhernandezjorge.localjobs.utils.User
import org.json.JSONArray
import org.json.JSONObject

object CloudController: AsyncResponse {
    private var offer = false
    private var company = false
    private var register = false
    private var registerCompany = false
    private var login = false
    private var stateOffers = false
    private var companyLogin = false
    private var companyOffers = false
    private var deletingOffer = false
    private var usersOfAOffer = false
    private var gettingUsers = false
    private var changingOfferState = false
    private var addingUserToAOffer = false
    private var gettingUser = false
    private var addingOffer = false
    private var responseArray = JSONArray()
    private var offersArray = JSONArray()
    private var userId = -1
    private var offerId = -1
    private var state = ""
    private lateinit var logUserFun: (Int) -> Unit
    private lateinit var logCompanyFun: (Int) -> Unit
    private var refreshFunction: ((Boolean) -> Unit)? = null
    var delegate: AsyncResponse? = null

    private const val SERVER = "https://localjobs-geocloud.herokuapp.com/"
    private const val USER_BY_ID = "userById"
    private const val LOGIN = "login"
    private const val OFFERS = "offers"
    private const val COMPANY = "company"
    private const val REGISTER = "register"
    private const val REGISTER_COMPANY = "registerCompany"
    private const val COMPANY_LOGIN = "companyLogin"
    private const val COMPANY_OFFERS = "companyOffers"
    private const val USERS_OFFER = "usersOffer"
    private const val DELETE_OFFER = "deleteOffer"
    private const val CHANGE_OFFER_STATE = "changeOfferState"
    private const val ADD_USER = "addUserToAOffer"
    private const val ADD_OFFER = "addOffer"
    private const val USER = "user"

    fun startApplication(userId: Int) {
        CloudController.userId = userId
        getJobOffers()
    }

    fun register(userJson: JSONObject, delegate: AsyncResponse) {
        this.delegate = delegate
        register = true

        requestServer("$REGISTER/$userJson")
    }

    fun registerCompany(companyJson: JSONObject, delegate: AsyncResponse) {
        this.delegate = delegate
        registerCompany = true

        requestServer("$REGISTER_COMPANY/$companyJson")
    }

    fun login(username: String, logUserFun: (Int) -> Unit) {
        CloudController.logUserFun = logUserFun
        login = true

        requestServer("$LOGIN/$username")
    }

    fun companyLogin(companyName: String, logCompanyFun: (Int) -> Unit) {
        CloudController.logCompanyFun = logCompanyFun
        companyLogin = true

        requestServer("$COMPANY_LOGIN/$companyName")
    }

    /**
     * Método para obtener todas las ofertas de trabajo disponibles
     */
    fun getJobOffers() {
        offer = true

        requestServer(OFFERS)
    }

    /**
     * Método para obtener el estado de las ofertas de un usuario en concreto
     */
    fun getUserOffers(userId: Int, refreshFunction: ((Boolean) -> Unit)? = null) {
        stateOffers = true
        CloudController.refreshFunction = refreshFunction

        requestServer("$OFFERS/$userId")
    }

    /**
     * Método para obtener todos los usuarios pertenecientes a una oferta
     */
    fun getUsersOfAOffer(offerId: Int, delegate: AsyncResponse) {
        usersOfAOffer = true
        CloudController.delegate = delegate

        requestServer("$USERS_OFFER/$offerId")
    }

    fun getCompanyOffers(companyId: Int, delegate: AsyncResponse) {
        companyOffers = true
        CloudController.delegate = delegate

        requestServer("$COMPANY_OFFERS/$companyId")
    }

    private fun getCompany(json: JSONObject) {
        val companyId = json.get(Utils.ID_EMPRESA) as Int

        responseArray.remove(0)
        requestServer("$COMPANY/$companyId")
    }

    private fun getUser(json: JSONObject) {
        gettingUsers = true
        val userId = json.get(Utils.ID_USUARIO) as Int

        responseArray.remove(0)
        requestServer("$USER_BY_ID/$userId")
    }

    fun addUserToAOffer(offerId: Int, username: String) {
        gettingUser = true
        CloudController.offerId = offerId

        requestServer("$USER/$username")
    }

    fun addUser() {
        addingUserToAOffer = true
        val json = JSONObject()

        json.accumulate(Utils.ID_OFERTA, offerId)
        json.accumulate(Utils.ID_USUARIO, userId)

        requestServer("$ADD_USER/$json")
    }

    fun addOffer(offer: Offer, delegate: AsyncResponse) {
        CloudController.delegate = delegate
        addingOffer = true
        val json = JSONObject()

        json.accumulate(Utils.TYPE, offer.type)
        json.accumulate(Utils.EXPERIENCE, offer.experience)
        json.accumulate(Utils.SALARY, offer.salary)
        json.accumulate(Utils.ID_EMPRESA, offer.companyId)

        requestServer("$ADD_OFFER/$json")
    }

    fun deleteOffer(id: Int) {
        deletingOffer = true
        offerId = id

        requestServer("$DELETE_OFFER/$id")
    }

    fun changeOfferState(offerId: Int, userId: Int, state: String, delegate: AsyncResponse) {
        changingOfferState = true
        CloudController.offerId = offerId
        CloudController.userId = userId
        CloudController.state = state
        CloudController.delegate = delegate
        val json = JSONObject()

        json.accumulate("ID_OFERTA", offerId)
        json.accumulate("ID_USUARIO", userId)
        json.accumulate("ESTADO", state)

        requestServer("$CHANGE_OFFER_STATE/$json")
    }

    private fun requestServer(arguments: String = "") {
        AsyncTask(this).execute("$SERVER$arguments")
    }

    override fun processFinish(response: String) {
        if(response.isNotEmpty()) {
            when {
                login || companyLogin -> {
                    val jsonArray = JSONArray(response)
                    val funToExecute = if(login) {
                        login = false
                        logUserFun
                    }
                    else {
                        companyLogin = false
                        logCompanyFun
                    }

                    if(jsonArray.length() > 0) {
                        val json = jsonArray[0] as JSONObject

                        funToExecute.invoke(json.getInt(Utils.ID))
                    }
                    else
                        funToExecute.invoke(-1)
                }

                //Primero descarga las ofertas, y según va descargando las ofertas, va descargando las empresas de esas ofertas
                offer -> {
                    offer = false
                    company = true
                    responseArray = JSONArray(response)
                    offersArray = JSONArray(response) //Guardamos array de ofertas para añadirlas una vez tengamos las empresas añadidas

                    if (responseArray.length() > 0) {
                        val json = responseArray[0] as JSONObject

                        getCompany(json)
                    }
                }

                //Se ejecuta desde 'offer' para ir obteniendo todas las empresas de cada oferta
                company -> {
                    val jsonArray = JSONArray(response)

                    if(jsonArray.length() > 0) {
                        val jsonCompany = jsonArray[0] as JSONObject

                        DBComunicator.getInstance().insertCompany(jsonCompany)
                    }

                    if (responseArray.length() > 0) {
                        val json = responseArray[0] as JSONObject

                        getCompany(json)
                    } else {
                        company = false
                        DBComunicator.getInstance().insertOffers(offersArray)
                        getUserOffers(userId)

                        println(DBComunicator.getInstance().getTableAsString(Utils.TABLA_EMPRESAS))
                        println(DBComunicator.getInstance().getTableAsString(Utils.TABLA_OFERTAS))

                        delegate!!.processFinish("Finish downloads")
                    }
                }

                companyOffers -> {
                    companyOffers = false
                    val jsonArray = JSONArray(response)

                    if(jsonArray.length() > 0) {
                        DBComunicator.getInstance().insertOffers(jsonArray)

                        delegate!!.processFinish("Finish downloads")
                    }
                }

                usersOfAOffer -> {
                    usersOfAOffer = false
                    responseArray = JSONArray(response)
                    println(responseArray)
                    if(responseArray.length() > 0) {
                        val json = responseArray[0] as JSONObject

                        state = json.getString(Utils.STATE)
                        getUser(json)
                    }
                }

                register -> {
                    register = false
                    val json = JSONObject(response)
                    val affectedRows = json.getInt("affectedRows")

                    if(affectedRows > 0)
                        delegate!!.processFinish("Ok")
                    else
                        delegate!!.processFailed()
                }

                registerCompany -> {
                    val json = JSONObject(response)
                    val affectedRows = json.getInt("affectedRows")

                    if(affectedRows > 0) {
                        DBComunicator.getInstance().insertCompany(json)
                        delegate!!.processFinish("Ok")
                    }

                    else
                        delegate!!.processFailed()
                }

                stateOffers -> {
                    stateOffers = false
                    val jsonArray = JSONArray(response)

                    if(jsonArray.length() > 0)
                        DBComunicator.getInstance().insertOffersStates(jsonArray)

                    if(refreshFunction != null) {
                        refreshFunction!!.invoke(true)
                        refreshFunction = null
                    }
                }

                deletingOffer -> {
                    deletingOffer = false
                    val json = JSONObject(response)
                    val affectedRows = json.getInt("affectedRows")

                    if(affectedRows > 0) {
                        DBComunicator.getInstance().deleteOffer(offerId)
                        offerId = -1
                        delegate!!.processFinish("Ok")
                    }

                    else
                        delegate!!.processFailed()
                }

                changingOfferState -> {
                    changingOfferState = false
                    val json = JSONObject(response)
                    val affectedRows = json.getInt("affectedRows")

                    if(affectedRows > 0) {
                        DBComunicator.getInstance().changeOfferState(offerId, userId, state)
                        offerId = -1
                        userId = -1
                        state = ""
                        delegate!!.processFinish("Ok")
                    }

                    else
                        delegate!!.processFailed()
                }

                gettingUsers -> {
                    gettingUsers = false
                    val jsonArray = JSONArray(response)

                    if(jsonArray.length() > 0) {
                        val jsonUser = jsonArray[0] as JSONObject

                        DBComunicator.getInstance().insertUser(jsonUser, state)
                    }

                    if(responseArray.length() > 0) {
                        val json = responseArray[0] as JSONObject

                        state = json.getString(Utils.STATE)
                        getUser(json)
                    }
                    else {
                        gettingUsers = false
                        delegate!!.processFinish("Ok")
                    }
                }

                gettingUser -> {
                    gettingUser = false
                    val jsonArray = JSONArray(response)

                    if(jsonArray.length() > 0) {
                        val json = jsonArray[0] as JSONObject

                        userId = json.getInt(Utils.ID)
                        addUser()
                    }
                }

                addingUserToAOffer -> {
                    addingUserToAOffer = false
                    val json = JSONObject(response)
                    val affectedRows = json.getInt("affectedRows")

                    if(affectedRows > 0) {
                        getUsersOfAOffer(offerId, delegate!!)
                        offerId = -1
                    }

                    else
                        delegate!!.processFailed()
                }

                addingOffer -> {
                    addingOffer = false
                    val json = JSONObject(response)
                    val affectedRows = json.getInt("affectedRows")

                    if(affectedRows > 0)
                        delegate!!.processFinish("Ok")

                    else
                        delegate!!.processFailed()
                }
            }
        }
        else
            setFlagsToFalse()
    }

    private fun setFlagsToFalse() {
        offer = false
        company = false
        register = false
        login = false
        stateOffers = false
        companyLogin = false
        companyOffers = false
        deletingOffer = false
        usersOfAOffer = false
        gettingUsers = false
        changingOfferState = false
        addingUserToAOffer = false
        gettingUser = false
        addingOffer = false
    }

    override fun processFailed() {

    }
}