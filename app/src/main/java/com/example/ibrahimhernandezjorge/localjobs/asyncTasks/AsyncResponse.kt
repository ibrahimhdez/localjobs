package com.example.ibrahimhernandezjorge.localjobs.asyncTasks

interface AsyncResponse {
    fun processFinish(response: String)
    fun processFailed()
}