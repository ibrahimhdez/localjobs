package com.example.ibrahimhernandezjorge.localjobs.activities

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import com.example.ibrahimhernandezjorge.localjobs.controllers.CloudController
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.adapters.CompanyOffersAdapter
import com.example.ibrahimhernandezjorge.localjobs.asyncTasks.AsyncResponse
import com.example.ibrahimhernandezjorge.localjobs.database.DBComunicator
import com.example.ibrahimhernandezjorge.localjobs.utils.CompanyOffers
import android.widget.Toast
import com.example.ibrahimhernandezjorge.localjobs.utils.Offer


class CompanyActivity: AppCompatActivity(), AsyncResponse {
    private var cloud = CloudController
    private var recycler: RecyclerView? = null
    private lateinit var adapter: CompanyOffersAdapter
    private lateinit var addOfferFab: FloatingActionButton
    private var offers = ArrayList<CompanyOffers>()
    private var companyId = -1
    private var deleting = false
    private var addingOffer = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.company_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initComponentes()
        startOfferDownload()
    }

    private fun initComponentes() {
        addOfferFab = findViewById(R.id.add_offer_fab)
        addOfferFab.setOnClickListener {
            showAddOfferDialog()
        }

        companyId = intent.extras!!.getInt(Utils.ID_EMPRESA)
        recycler = findViewById(R.id.company_recycler)
        recycler!!.layoutManager = LinearLayoutManager(this)
    }

    private fun startOfferDownload() {
        cloud.getCompanyOffers(companyId, this)
    }

    private fun refreshAdapter() {
        offers = getOffers()
        adapter = CompanyOffersAdapter(offers, ::showOffersMenu)
        recycler!!.adapter = adapter

        adapter.notifyDataSetChanged()
    }

    private fun getOffers(): ArrayList<CompanyOffers> {
        val offers = ArrayList<CompanyOffers>()
        val cursor = DBComunicator.getInstance().cursorConsulta(Utils.TABLA_OFERTAS, arrayOf(Utils.ID_EMPRESA), arrayOf(companyId.toString()))

        while(cursor.moveToNext()) {
            val id = cursor.getInt(cursor.getColumnIndex(Utils.ID))
            val type = cursor.getString(cursor.getColumnIndex(Utils.TYPE))
            val salary = cursor.getString(cursor.getColumnIndex(Utils.SALARY))
            val experience = cursor.getString(cursor.getColumnIndex(Utils.EXPERIENCE))
            val offer = CompanyOffers(id = id, type = type, salary = salary, experience = experience)

            offers.add(offer)
        }

        return offers
    }

    fun showOffersMenu(card: CardView, position: Int) {
        card.setOnLongClickListener { _ ->
            val popup = PopupMenu(this, card)
            val inflater = popup.menuInflater

            inflater.inflate(R.menu.company_offer_menu, popup.menu)
            popup.show()

            popup.menu.findItem(R.id.show_candidates).setOnMenuItemClickListener {
                val intent = Intent(applicationContext, UsersOfOffersActivity::class.java)

                intent.putExtra(Utils.ID, offers[position].id)
                startActivity(intent)
                true
            }

            popup.menu.findItem(R.id.delete_offer).setOnMenuItemClickListener {
                deleting = true
                cloud.deleteOffer(offers[position].id)
                true
            }
            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.refresh_action, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.action_refresh -> startOfferDownload()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showAddOfferDialog() {
        val layoutView = View.inflate(applicationContext, R.layout.add_offer_dialog, null)
        val experienceRadioGroup = layoutView.findViewById<RadioGroup>(R.id.experience_radioGroup)
        val salaryRadioGroup = layoutView.findViewById<RadioGroup>(R.id.salary_radioGroup)
        val typeOfferEditText = layoutView.findViewById<EditText>(R.id.type_offer_editText)

        val builder = AlertDialog.Builder(this)
        builder.setView(layoutView)
                .setCancelable(true)
                .setPositiveButton("Add") { _, _ ->
                    val idRadioButtonExperience = experienceRadioGroup.checkedRadioButtonId
                    val idRadioButtonSalary = salaryRadioGroup.checkedRadioButtonId

                    val type = typeOfferEditText.text.toString().toUpperCase()
                    val experienceSelected = experienceRadioGroup.findViewById<RadioButton>(idRadioButtonExperience).text.toString()
                    val salarySelected = salaryRadioGroup.findViewById<RadioButton>(idRadioButtonSalary).text.toString()
                    val offer = Offer(-1, type, salarySelected, experienceSelected, companyId)

                    addingOffer = true
                    cloud.addOffer(offer, this)
                }.setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
        builder.show()
    }

    override fun processFinish(response: String) {
        if(deleting) {
            deleting = false
            Toast.makeText(applicationContext, "Offer deleted correctly!", Toast.LENGTH_LONG).show()
        }

        else if(addingOffer) {
            addingOffer = false
            startOfferDownload()
        }

        refreshAdapter()
    }

    override fun processFailed() {
        Toast.makeText(applicationContext, "Error deleting offer!", Toast.LENGTH_LONG).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
}