package com.example.ibrahimhernandezjorge.localjobs.asyncTasks

import android.os.AsyncTask
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.net.URLConnection
import java.nio.charset.Charset.defaultCharset

class AsyncTask(private var delegate: AsyncResponse): AsyncTask<String, Void, String>() {//, private var spotsDialog: SpotsDialog): AsyncTask<String, Void, String>() {

    override fun doInBackground(vararg params: String?): String {
        val stringbuilder = StringBuilder()
        val urlConn: URLConnection?
        var inputStreamReader: InputStreamReader? = null
        val url = URL(params[0])
        println("URL: ${params[0]}")

        try {
            urlConn = url.openConnection()

            if (urlConn?.getInputStream() != null) {
                inputStreamReader = InputStreamReader(urlConn.getInputStream(), defaultCharset())
                val bufferedReader = BufferedReader(inputStreamReader)
                var cp = 0

                while ({ cp = bufferedReader.read(); cp }() != -1)
                    stringbuilder.append(cp.toChar())

                bufferedReader.close()
            }

            inputStreamReader!!.close()
        } catch (e: Exception) {
            println("EXPCEPTION: $e")
            //spotsDialog.dismiss()
        }
        return stringbuilder.toString()
    }

    /**
     * Función que llama al método processFinish de la clase que llamó a esta tarea asíncrona
     * @param consulta
     */
    override fun onPostExecute(result: String) {
        delegate.processFinish(result)
    }
}