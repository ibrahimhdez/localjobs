package com.example.ibrahimhernandezjorge.localjobs.database

import android.content.Context

/**
 * Clase que actúa de comunicador para que todos los paquetes accedan a la misma instancia de Base de Datos
 * @author Ibrahim Hernández Jorge
 */
class DBComunicator {
    companion object {
        /**
         * Instancia privada. Se puede acceder a ella mediante [getInstance].
         */
        private lateinit var dbInstance: DBHelper


        /**
         * Método para inicializar la base de datos
         */
        @JvmStatic
        @Synchronized
        fun initInstance(context: Context) {
            dbInstance = DBHelper(context)
        }

        /**
         * Accede a la instancia de la base de datos.
         * Debe haber sido inicializada previamente con [initInstance]
         */
        @JvmStatic
        @Synchronized
        fun getInstance(): DBHelper {
            return dbInstance
        }
    }
}