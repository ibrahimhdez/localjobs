package com.example.ibrahimhernandezjorge.localjobs.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.ibrahimhernandezjorge.localjobs.R
import com.example.ibrahimhernandezjorge.localjobs.Utils
import com.example.ibrahimhernandezjorge.localjobs.asyncTasks.AsyncResponse
import com.example.ibrahimhernandezjorge.localjobs.controllers.CloudController
import com.example.ibrahimhernandezjorge.localjobs.controllers.LocationController
import org.json.JSONObject

class RegisterActivity: AppCompatActivity(), AsyncResponse {
    private var cloud = CloudController
    private lateinit var locationController: LocationController
    private lateinit var firstTextInputLayout: TextInputLayout
    private lateinit var secondTextInputLayout: TextInputLayout
    private lateinit var thirdTextInputLayout: TextInputLayout
    private lateinit var firstEditText: EditText
    private lateinit var secondEditText: EditText
    private lateinit var thirdEditText: EditText
    private lateinit var registerButton: Button
    private var registerType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        registerType = intent.extras!!.getString(Utils.TYPE)
        initComponentes()
    }

    private fun initComponentes() {
        locationController = LocationController(this)

        firstTextInputLayout = findViewById(R.id.first_textInputLayout)
        secondTextInputLayout = findViewById(R.id.second_textInputLayout)
        thirdTextInputLayout = findViewById(R.id.third_textInputLayout)

        firstEditText = findViewById(R.id.first_edittext)
        secondEditText = findViewById(R.id.second_edittext)
        thirdEditText = findViewById(R.id.third_edittext)

        registerButton = findViewById(R.id.to_register_button)

        if(registerType == Utils.TABLA_EMPRESAS) {
            firstTextInputLayout.hint = "Name"
            secondTextInputLayout.hint = "Address"
            thirdTextInputLayout.hint = "Type"
        }
        else if(registerType == Utils.TABLA_USUARIOS) {
            firstTextInputLayout.hint = "Username"
            secondTextInputLayout.hint = "Name"
            thirdTextInputLayout.hint = "Surname"
        }

        registerButton.setOnClickListener {
            val json = JSONObject()
            val firstData = firstEditText.text.toString().toUpperCase()
            val secondData = secondEditText.text.toString().toUpperCase()
            val thirdData = thirdEditText.text.toString().toUpperCase()

            if(registerType == Utils.TABLA_EMPRESAS) {
                val companyLocation = locationController.getCoordinates(secondData)
                val latitud = companyLocation?.latitude
                val longitud = companyLocation?.longitude

                json.accumulate(Utils.NAME, firstData)
                json.accumulate(Utils.ADDRESS, secondData)
                json.accumulate(Utils.LATITUD, latitud)
                json.accumulate(Utils.LONGITUD, longitud)
                json.accumulate(Utils.TYPE, thirdData)

                cloud.registerCompany(json, this)
            }
            else if(registerType == Utils.TABLA_USUARIOS) {
                json.accumulate(Utils.USERNAME, firstData)
                json.accumulate(Utils.NAME, secondData)
                json.accumulate(Utils.SURNAME, thirdData)

                cloud.register(json, this)
            }
        }
    }

    override fun processFinish(response: String) {
        Toast.makeText(applicationContext, "Registration successfully!", Toast.LENGTH_LONG).show()

        val intent = Intent(application, LoginActivity::class.java)

        startActivity(intent)
    }

    override fun processFailed() {
        Toast.makeText(applicationContext, "Registration failed!", Toast.LENGTH_LONG).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
}