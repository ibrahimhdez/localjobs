package com.example.ibrahimhernandezjorge.localjobs.utils

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

class Company(var id: Int, var name: String, var position: LatLng, var marker: Marker)